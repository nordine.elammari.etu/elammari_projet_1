# Projet de commande tree FTP

## Nom

Nordine El ammari 

## lancement du programme

- lancer les commandes suivantes :

``` code
mvn package 
java -jar ./target/ftp-1.0.jar <adresse_du_serveur_ftp> (optionnel) <user> <password>
```
- exemples:
``` code
java -jar ./target/ftp-1.0.jar ftp.free.fr
```

ou

``` code
java -jar ./target/ftp-1.0.jar ftp.free.fr anonymous anonymous
```
- génerer la javadoc :

``` code
mvn javadoc:javadoc
```

vous pouvez visualiser la javadoc html génerer en ouvrant à l'aide d'un navigateur target/docs/index.html

## Résumé 

(début de l'implémentation du client ftp avec pour but final d'afficher sous forme de commande tree linux l'arborescence de fichiers d'un serveur FTP.)

L'objectif du projet est de mettre en œuvre un commande shell permettant d'afficher sur la sortie standard d'un terminal  l'arborescence d'un répertoire distant accessible via le protocole applicatif File Transfer Protocol (FTP). Le rendu de l'arborescence distante s'inspirera du formalisme utilisé la commande tree de Linux.

Cette nouvelle commande tree-ftp prend un argument obligatoire en paramètre: l'adresse du serveur FTP distant. Le 2e argument—optionnel—permet d'indiquer le nom d'utilisateur à utiliser, le 3e—optionnel également—correspond au mot de passe. Il n'y a aucune autre interaction de l'utilisateur avec la commande qui est nécessaire au delà de l'exécution avec les paramètres décrits ci-dessus.

## Architecture

![](doc/uml.png)

## Code Samples

Récursivité de la méthode Tree:

``` JAVA
/**
     * method to print the files tree
     * @param depth The depth in the files tree
     */ 
    public synchronized void tree(int depth) {
    	List<String> root = null;
    	try {
    		root = this.list();
    	}
    	catch (Exception e) {
    		throw new RuntimeException(e);
    	}
    	for (String elem : root) {
		String filename;
		if (this.isLink(elem))  filename = this.getLink(elem);
		else  filename = this.getFileName(elem);
    		String tabs = this.tabBuilder(depth);
    		if (root.indexOf(elem) != (root.size() -1)) {
    			System.out.println(tabs + "├── " + filename);
    		}  
    		else {
    			System.out.println(tabs + "└── " + filename);
    		}
    		
    		if (isDirectory(elem)) {
    			try {
	    			this.cwd(filename);
	    			this.tree(depth+1);
	    			this.cwd("..");
    			}
    			catch(Exception e) {
    				//throw new RuntimeException(e);
    			}
    		}
    	}
    }
```
