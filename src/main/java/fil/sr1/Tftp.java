package fil.sr1;

import java.io.*;
import java.net.Socket;
import java.util.ArrayList;
import java.util.List;
import java.util.StringTokenizer;

/**
 * @author Nordine El ammari
 * created on 01/2021
 */
public class Tftp {
    private Socket socket = null;
    private BufferedReader reader = null;
    private BufferedWriter writer = null;
    private String host;
    private int port;
    private String user;
    private String pass;
    private Socket dataSocket = null;
    

    

    public Tftp(String host, int port, String user, String pass) throws IOException {
        this.host=host;
        this.port=port;
        this.user=user;
        this.pass=pass;
        
    }
    
    public Tftp(String host, int port) throws IOException {
    	this(host, port, "anonymous", "anonymous");
    }
  
    public Tftp(String host) throws IOException {
        this(host, 21, "anonymous", "anonymous");
    }
    /**
     * connection to the server
     * @throws IOException
     */
    public synchronized void connect() throws IOException {
    	if (socket != null) {
            throw new IOException("Tftp est dejà connecte à un FTP");
        }
        
        socket = new Socket(host, port);
        reader = new BufferedReader(new InputStreamReader(socket.getInputStream()));
        writer = new BufferedWriter(new OutputStreamWriter(socket.getOutputStream()));

        String reponse = readLine();
        //Code 220 - Service disponible 
        if (!reponse.startsWith("220 ")) throw new IOException("FTP indisponible -> " + reponse);

        sendLine("USER " + user);
        reponse = readLine();
        //Code 331 - accuse de reception du nom d'utilisateur, en attente du mot de passe.
        if (!reponse.startsWith("331 ")) throw new IOException("nom d'utilisateur incorrect -> " + reponse);

        sendLine("PASS " + pass);
        reponse = readLine();
        if (!reponse.startsWith("230 ")) throw new IOException("mot de passe incorrect -> " + reponse);
    } 
	
    /**
     * disconnection 
     * @throws IOException
     */
    public synchronized void disconnect() throws IOException {
        try {
            sendLine("QUIT");
        } finally {
            socket.close();
            this.socket=null;
            
        }
    }

    private void sendLine(String line) throws IOException {
        if (socket == null) {
            throw new IOException("Tftp n'est pas connecte au FTP");
        }
        try {
            writer.write(line + "\r\n");
            writer.flush();
        } catch (IOException e) {
            socket = null;
            throw e;
        }
    }

    private String readLine() throws IOException {
        String line = reader.readLine();
        return line;
    }
    
	private int type(String elem){
        if(elem.startsWith("-")) return 0;
        if(elem.startsWith("d")) return 1;
        if(elem.startsWith("l")) return 2;
        return 0;
    }

    /**
     * Return true if elem is a link
     */
    public boolean isLink(String elem){
    	if (type(elem)==2) return true;
	else return false;
    }

    /**
     * Return true if elem is a directory
     */
    public boolean isDirectory(String elem) {
    	if (type(elem)==1) return true;
    	else return false;
    }
    
    /**
     * method to get a dir or file name
     * @param lsLine the line given by the method list
     * @return the name of the file or dir name
     **/
    protected String getFileName(String lsLine) {
		String[] splited = lsLine.split(" ");
		return splited[splited.length - 1];
	}
    /**
     * method to display link name and link pointer
     * @param lsLine the line given by the method list
     * @return the name of the link and an arrow which point on the dir of the link
     */
    protected String getLink(String lsLine) {
	        String[] splited = lsLine.split(" ");
		return splited[splited.length - 3]+" "+splited[splited.length - 2]+" "+splited[splited.length - 1];
    }

    public synchronized String pwd() throws IOException {
        String dir = null;
        sendLine("PWD");
        String reponse = readLine();
        if (reponse.startsWith("226")) reponse = readLine();
        if (reponse.startsWith("257 ") ) {
        	//Obtention du repertoire entre deux quotes
            int firstQuote = reponse.indexOf('\"');
            int secondQuote = reponse.indexOf('\"', firstQuote + 1);
            if (secondQuote > 0) {
                dir = reponse.substring(firstQuote + 1, secondQuote);
            }
        }
        return dir;
    }

    /**
     * Commande pour changer de repertoire "CHANGE WORKING DIRECTORY"
     * @param dir Dossier destinataire
     * @return True si on a bien changer de repertoire ou False sinon
     * @throws IOException
     */
    public synchronized boolean cwd(String dir) throws IOException {
        sendLine("CWD " + dir);
        String response = readLine();
        //Code 250 - Service fichier termine.
        return (response.startsWith("250 "));
    }
    
    private synchronized void pasv() throws IOException{
    	sendLine("PASV");
        String reponse = readLine();
        if (reponse.startsWith("250")){
        	reponse = readLine();
    }
        
        if (!reponse.startsWith("227")) {
            throw new IOException("TreeFTP ne peut pas passer en mode passif: " +reponse);
        }

        String ip = null;
        int port = -1;
        int opening = reponse.indexOf('(');
        int closing = reponse.indexOf(')', opening + 1);
        if (closing > 0) {
            String dataLink = reponse.substring(opening + 1, closing);
            StringTokenizer tokenizer = new StringTokenizer(dataLink, ",");
            try {
                ip = tokenizer.nextToken() + "." + tokenizer.nextToken() + "."
                        + tokenizer.nextToken() + "." + tokenizer.nextToken();
                port = Integer.parseInt(tokenizer.nextToken()) * 256
                        + Integer.parseInt(tokenizer.nextToken());
            } catch (Exception e) {
                throw new IOException("TreeFTP n'a pas reussi à connecter aux donnees: " + reponse);
            }
        }

        this.dataSocket = new Socket(ip, port);
        
    }

    /**
     * Liste des fichiers du dossier actuel
     * @return reponse
     * @throws IOException
     */
    public synchronized List<String> list() throws IOException {
    	this.pasv();
        sendLine("LIST");
        String reponse = readLine();
        if (reponse.startsWith("425 ")) throw new IOException("Use PORT or PASV before LIST");

        BufferedReader input = new BufferedReader(new InputStreamReader(this.dataSocket.getInputStream()));
        ArrayList<String> files = new ArrayList<>();
        String nextLine = input.readLine();
        while(nextLine != null){
        	//if (nextLine.startsWith("-") || nextLine.startsWith("l")){
        		files.add(nextLine);
        	//}
        	/*else if(nextLine.startsWith("d")) {
        		List<String> list = Arrays.asList(nextLine.split("\\s* \\s*"));
        		String directory = list.get(list.size()-1);
        		System.out.println(directory);
        		this.cwd(directory);
        		String subFiles = this.list(depth+1);
        		this.cwd("..");
        		//this.dataSocket = saveSocket;
        		
        	}*/
            nextLine = input.readLine();
        }
        input.close();
        return files;
    }
    /**
     * method to print the files tree
     * @param depth The depth in the files tree
     */ 
    public synchronized void tree(int depth) {
    	List<String> root = null;
    	try {
    		root = this.list();
    	}
    	catch (Exception e) {
    		throw new RuntimeException(e);
    	}
    	for (String elem : root) {
		String filename;
		if (this.isLink(elem))  filename = this.getLink(elem);
		else  filename = this.getFileName(elem);
    		String tabs = this.tabBuilder(depth);
    		if (root.indexOf(elem) != (root.size() -1)) {
    			System.out.println(tabs + "├── " + filename);
    		}  
    		else {
    			System.out.println(tabs + "└── " + filename);
    		}
    		
    		if (isDirectory(elem)) {
    			try {
	    			this.cwd(filename);
	    			this.tree(depth+1);
	    			this.cwd("..");
    			}
    			catch(Exception e) {
    				//throw new RuntimeException(e);
    			}
    		}
    	}
    }

    /**
     * Method to have the good tabulation for the construction of the tree
     * @param depth the depth in the files tree
     * @return The string with the good number of tabs
     */
    private String tabBuilder(int depth) {
    	StringBuilder sb = new StringBuilder();

		for (int i = 0; i < depth; i++) {
			sb.append("│   ");
		}
		return sb.toString();
	}

	public static void main(String[] args) throws IOException {
        String address = null;
       	if (args.length != 1 && args.length !=3) {
		System.out.println("Usage : 1 argument the ftp address if your FTP host doesn't need user and password \n"
			               + "\t2 more arguments the username and the password else ");
	      	System.exit(0);	
	}
	else address = args[0];
        Tftp Tftp = null;
        if (args.length == 1) {
	        Tftp = new Tftp(address);
        	}
        else if (args.length == 3) {
        	Tftp = new Tftp(address, 21, args[1], args[2]);
	        
        }
        try {
			Tftp.connect();
		} catch (Exception e) {
			throw new RuntimeException(e);
			}
        
        Tftp.tree(0);
        //Tftp.disconnect();
	}
	
}

