package fil.sr1;

import static org.junit.Assert.assertTrue;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertEquals;

import org.junit.Test;
import org.junit.Before;

/**
 * Unit test for Tftp.
 */
public class TftpTest 
{

	private Tftp ftp;
	
	@Before
	public void init() {
		try{
		ftp = new Tftp("ftp.free.fr");
		}
		catch(Exception e){
			System.err.println("n'a pu se connecter");
		}
    }
    /**
     * Rigorous Test :-)
     */
    	@Test
    	public void testReturnTrueWhenLink(){
		String line = "lrwxrwxrwx    1 ftp      ftp            31 Sep 11  2018 kali-security -> ../.mirrors15/security.kali.org";
		assertTrue(ftp.isLink(line));
	}

	@Test
	public void testReturnFalseWhenNotLink(){
		String line = "drwxr-xr-x    3 ftp      ftp          4096 May 26  2009 support-alice";
		assertFalse(ftp.isLink(line));
	}

	@Test
	public void testReturnTrueWhenDirectory(){
		String line = "drwxr-xr-x    3 ftp      ftp          4096 May 26  2009 support-alice";
		assertTrue(ftp.isDirectory(line));
	}

	@Test
    	public void testReturnFalseWhenNotDirectory(){
		String line = "lrwxrwxrwx    1 ftp      ftp            31 Sep 11  2018 kali-security -> ../.mirrors15/security.kali.org";
		assertFalse(ftp.isDirectory(line));
	}

	@Test
	public void testGetLink(){
		String line = "lrwxrwxrwx    1 ftp      ftp            31 Sep 11  2018 kali-security -> ../.mirrors15/security.kali.org";
		String link = ftp.getLink(line);
		assertEquals(link, "kali-security -> ../.mirrors15/security.kali.org");
	}
}
